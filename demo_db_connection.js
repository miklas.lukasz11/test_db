var mysql = require('mysql');

var con = mysql.createConnection({
  host: "0.0.0.0",
  user: "root",
  password: "root",
  insecureAuth : true
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});